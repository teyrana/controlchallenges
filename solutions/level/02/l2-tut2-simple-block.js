// for use with: https://janismac.github.io/ControlChallenges/
//       source: https://github.com/janismac/ControlChallenges

// Settle Time === 4.04 // PR

const kp = -5.;
const kd = -3.;
const ki = 0;
const delta_t = 0.02; // The simulation time step

let ft = 0; // force, total
let fp; // force, proportional
let fi; // force, integrative
let fd; // force, derivative
let integral = 0;

function controlFunction(block) {
    fp = kp * block.x;
    //integral = 0.99 * (integral + block.dx);
    integral += delta_t * block.x;  
  
    fi = ki * integral;
    fd = kd * block.dx;

    monitor( "Force, Total", ft );
    monitor( "    fp", fp );
    monitor( "    fi", fi );
    monitor("      int:", integral)
    monitor( "    fd", fd );
  
    return fp + fi + fd;
}
