// for use with: https://janismac.github.io/ControlChallenges/
//       source: https://github.com/janismac/ControlChallenges

// Settle Time:

// Simulates for 0.5 seconds. The input for the model is set to be the negative of its `x`.
// ```
//     newState = state.simulate(0.5, function(s){ return -s.x ;})
// ```
// simulate() returns a modified copy of the model state.
// The control function will be evaluated only once at the start of a simulate() call
// and held constant during the simulation, so choose a small dt.

// ==================================================================
// Land the rocket in the landing zone. The center of the landing zone has the 
// coordinates (x, y) = (0, 0). Steer the rocket by calculating the engine 
// thrust (range 0 to 1) and engine steering angle (range -0.2 to 0.2 radians). 
// The rocket has a thrust to weight ratio of 2. A throttle value of 0.5 can make 
// it hover. Touch down gently (less than 5 m/s).

// 
// ???     TWR: 2,
//   throttle: 1,
//      theta: 0,
//      dtheta: 0,
//      gimbalAngle: -0.1,
//      Length: 40,
//      Width: 2,
//      x: 0,
//      dx: 0,
//      y: 0,
//      dy: 0,
//      throttleLimit: 0,
//      T: 0,
//      landingConstraints: {dx:5,dy:5,dtheta:0.1,sinTheta:0.05},

// ====== ====== Utility Functions: ====== ====== 
function clamp(num, min, max) {
  return Math.min(Math.max(num, min), max);
};

// ====== ====== Constants: ====== ====== 
const throttleLimits = {min:0, max:1.0};
const gimbalLimits = {min:-0.2, max:0.2};

const maxSinkRate = -20.0;
const scaleSinkRate = -0.16;

// # Constants
// ## Gimbal
// ### Target Theta Angle
const kxp = -0.008;
// ### Theta Angle
const kgthp = -2.5;  // Proportional
const kgthd =  2.8;  // Derivative

const kxd = 0.1;

// ## Throttle
// ### Angle
const ktgp = 4.0;
const ktap = 8.0;
// ### Position:
const kttp = 1.2;
const kttd = 0.01;
const ktyp = 2.0;


// ====== ====== Control Function: ====== ====== 
// state_vector = [.x, .dx, .y, .dy, .theta, .dtheta]
function controlFunction(x){
    let u = {throttle:0, gimbalAngle:0.0};

    // ====== Gimbal Terms: ======
    // ## Define a target theta:
    // ### proportional term
    let target_theta_x_p = clamp(kxp * x.x, -0.6, 0.6);
    monitor("      :gimbal:target-theta:prop:   ", target_theta_x_p );

    let gimbal_theta_prop = kgthp * (target_theta_x_p - x.theta);
    monitor("      :gimbal:theta:prop:          ", gimbal_theta_prop );

    let gimbal_theta_deriv = kgthd * x.dtheta;  
    monitor("      :gimbal:theta:deriv:         ", gimbal_theta_deriv );
  
    // ### derivative term
  	let gimbal_theta_dx_d = kxd*x.dx;
    monitor("      :gimbal:target-theta:deriv:  ", gimbal_theta_dx_d );

    u.gimbalAngle = gimbal_theta_prop + gimbal_theta_deriv + gimbal_theta_dx_d;
    monitor("    :gimbal:theta:            ", u.gimbalAngle );

    // ====== Throttle Terms: ======
    let throttle_gimbal_prop = ktgp * Math.abs(u.gimbalAngle);
    monitor("      :Throttle:gimbal:prop:       ", throttle_gimbal_prop );

    let throttle_travel_prop = ktap * Math.abs(x.theta);
    monitor("      :Throttle:travel:prop:       ", throttle_travel_prop );


    // adjust descent rate based on current height
    let targetSinkRate = clamp(scaleSinkRate*x.y, maxSinkRate, 0);
    monitor("      :Throttle:target-dy:         ", targetSinkRate );

    let err_dy = targetSinkRate - x.dy;
    monitor("      :Throttle:err(dy):           ", err_dy );
    let throttle_e_dy_prop = clamp( ktyp * err_dy, 0.1, 1);
    monitor("      :Throttle:e(dy):prop:        ", `${ktyp*err_dy} => ${throttle_e_dy_prop}` );

    u.throttle = throttle_gimbal_prop + throttle_e_dy_prop;
    monitor("    :Throttle:               ", u.throttle );

    return u;
}

