// for use with: https://janismac.github.io/ControlChallenges/
//       source: https://github.com/janismac/ControlChallenges

// Settle Time:
// ( -2, -1.2, -24) == 8.14 
// ( -3, -2, -28) == 10.

// slower, but more robust:
//   (-3, -1.5, -26, 0.99) == 18.32
//   (-9, -1.5, -50, 0.99) == 14.9
// * (-8, -1.5, -44, 0.99) == 13.7
// * (-8, -1.6, -44, 0.99) == 13.7
//   (-8, -1.8, -44, 0.99) == 16.84

const kp = -2;
const ki = -1.2;
const kd = -24;
const decay = 0.99;

const dt = 0.02; // The simulation time step

let fp; // force, proportional
let integral = 0;
let fi; // force, integrative
let fd; // force, derivative
let ft = 0; // force, total

function controlFunction(block) {
    fp = kp * block.x;

    // according to "standard theory", the integral term is: 
    //    \[ = K_i \int_0^t e(t) \,dt \]
    // Where `e` is the process variable, the difference between the setpoint and the measured process variable.
    integral = 0.98 * (integral + block.x);

    fi = ki * integral;
    fd = kd * block.dx;

    monitor( "Force, Total", ft );
    monitor( "    fp", fp );
    monitor( "    fi", fi );
    monitor("      int:", integral)
    monitor( "    fd", fd );
  
    return fp + fi + fd;
}
