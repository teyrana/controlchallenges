// for use with: https://janismac.github.io/ControlChallenges/
//       source: https://github.com/janismac/ControlChallenges

// Settle Time:

// Simulates for 0.5 seconds. The input for the model is set to be the negative of its `x`.
// ```
//     newState = state.simulate(0.5, function(s){ return -s.x ;})
// ```
// simulate() returns a modified copy of the model state.
// The control function will be evaluated only once at the start of a simulate() call
// and held constant during the simulation, so choose a small dt.

// ==================================================================

const kasp = -2.0; // constant, angle, strike, proportional
const kaxp = 0.1; // constant, angle, dx, proportional
const kad = 1.0;
let angle_p;
let angle_d;
let angle_command;

const kpp = 0.0;
const kpd = 0.0;
let piston_p;
let piston_d;
let piston_command;

let ctrl = {
    // range: [-10.0, 10.0]
    pistonAcceleration:0,
    // range: [-15.0, 15.0]
    hingeAcceleration:0,
}

function controlFunction(ball, piston, hinge, T)
{
    // ball: .x, .y
    //       .vx, .vy
    // piston: .length   [1.5, 2.3]
    //         .speed
    //         .base_y === -5
    //  hinge: .angle
    //         .speed // (angular_speed) [-0.7, +0.7]

    // pos = {x:ball.x, y:ball.y}
    let dx = ball.x
    let dy = ball.y - piston.length
    
    let strike_angle = Math.atan( ball.vx/ball.vy );
    let target_angle;
    if( 0 < strike_angle ){
      target_angle = (Math.PI/2 - strike_angle);
    }else{
      target_angle = (-Math.PI/2 - strike_angle);
    }
    let d_angle_strike_prop = kasp * target_angle;
  	let d_angle_dx_prop = kaxp * ball.x;
    let d_angle_deriv = kad * hinge.speed;

    ctrl.hingeAcceleration = d_angle_strike_prop + d_angle_dx_prop + d_angle_deriv; 

    //monitor("    :dpos:    ", {x: dx, y: dy} );
    monitor("    :strike_angle:     ", strike_angle);
    monitor("    :target_angle:     ", target_angle);
    monitor("    :hinge.angle.strike.prop: ", d_angle_strike_prop);
    monitor("    :hinge.angle.dx.prop: ", d_angle_dx_prop);
    monitor("    :hinge.angle.derv: ", d_angle_deriv);
    monitor("    :hinge.acc:        ", ctrl.hingeAcceleration );

    ctrl.pistonAcceleration = 0;

    //monitor("Throttle, Total: ", ctrl );
  
    return ctrl;
}
