// for use with: https://janismac.github.io/ControlChallenges/
//       source: https://github.com/janismac/ControlChallenges

// Settle Time:

// Simulates for 0.5 seconds. The input for the model is set to be the negative of its `x`.
// ```
//     newState = state.simulate(0.5, function(s){ return -s.x ;})
// ```
// simulate() returns a modified copy of the model state.
// The control function will be evaluated only once at the start of a simulate() call
// and held constant during the simulation, so choose a small dt.

// ==================================================================


// # Motion Model
//
// By inspecting js/models/SinglePendulum.js, this is what I think the motion model is:
// Assumes:
//   1. M >> m
//   2. $\theta$ ~= 0  (i.e., that we linearized around zero, with the small angle assumption)
//
// ## Rotation (Theta)
//   \[ \ddot{\theta} = \frac{g}{L} \sin \theta - \frac{F]{m L^2} \]
// 
// ... 
// \[ f = m * L^2 ( (g/L) sin(theta) - ddtheta )
//
//
// # Linear Quadratic Regulator (LQR)
// 
// (from Matlab / Octave):
// Q = diag([ 1, 1, 10, 10]);
// R = 0.01;
// K = lqr(A,B,Q,R);
//
// ## Results
// For our parameters and initial conditions, this predicts
// K = -10.0000   -24.5754  -332.9273  -111.5153

// ....
// <hr>
// Old notes follow....
// 
// ## Cost Function:
// (2) \[ J = \int_0^\ifty x^T Q x + u^T R u ) \,dt  \]
//
// ## Optimal Control Law
// \[ u = -K x \]
// Where `K` is obtained from solving the discrete-time algebraic Riccati equation (DARE).
//
// ## Closed Loop Control System
// From (1)... 
// \[   \dot{x} = (A − B K )x  \]
//   (if (A-BK) has negtive real eigenvalues, the system is stable)
// 

// example 1:
//     https://de.mathworks.com/help/slcontrol/ug/control-of-an-inverted-pendulum-on-a-cart.html
// example 2:
//     http://www.csd.elo.utfsm.cl/simulations/pend_sim.html
//     http://www.csd.elo.utfsm.cl/simulations/pend_sim3.html


// const decay = 0.99;
const length = 1.0;
const g = 9.81;
const m0 = 10;
const m1 = 0.5;
const dt = 0.02; // The simulation time step

// ## Stage 1: Manual Tuning:
// const kpa = 150.0;
// const kda = 20.;

// const kpx = 15.0;
// const kdx = 30.0;

// ### Best: 6.16 sec @ 
//     - kpa = 320.0;
//     - kda = 80.0;
//     - kpx = 15.0;
//     - kdx = 30.0;

// ## Stage 2: Optimal Tuning via LQR:
const kpx = -10.0000;
const kdx = -24.5754;
const kpa = -332.9273;
const kda = -111.5153;

// ### Best: 5.4 sec:
//     - kpx = 10.0000;
//     - kdx = 24.5754;
//     - kpa = 332.9273;
//     - kda = 111.5153;

let xlimits = { min:0, max:-10 };

function controlFunction(x){
    
    // xlimits.min = Math.min(xlimits.min, p.x );
    // xlimits.max = Math.max(xlimits.max, p.x );
    // monitor("    :x:limits:        ", `min:${xlimits.min}, max:${xlimits.max}`);

    let force_angle_prop = kpa * x.theta;
    let force_angle_deriv = kda * x.dtheta;
    monitor("      :force.prop:      ", force_angle_prop );
    monitor("      :force.deriv:     ", force_angle_deriv );
  
    let force_dx_prop = kpx*x.x;
    let force_dx_deriv = kdx*x.dx;
    monitor("      :force_dx_prop:   ", force_dx_prop );
    monitor("      :force_dx_deriv:  ", force_dx_deriv );

    let force_total = force_angle_prop + force_angle_deriv + force_dx_prop + force_dx_deriv;
    monitor("    :force:[-50,50]:  ", force_total );

    return force_total
}
