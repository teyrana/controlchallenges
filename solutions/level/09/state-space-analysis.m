#!/usr/bin/octave -qf

% 1. Started from:
%     https://chat.openai.com/c/5cd5860f-5dc1-434d-b8c2-fba4f4eede2e
# 2. Verified and fixed from:
#     "Modern Control Systems", 13th Ed. Dorn & Bishop, pp179-180

% Define system parameters
M = 10.0;  % Mass of the cart
m = 0.5;  % Mass of the pendulum
b = 0.0;  % Friction coefficient
l = 1.0;  % Length of pendulum arm
g = 9.81; % Gravity acceleration

% Define state-space matrices
% \[ x := [ x, \dot x, \theta, \dot \theta \]
A = [ 0    1         0    0;
      0    0  -(m*g)/M    0;
      0    0         0    1;
      0    0       g/l    0];
B = [0; 1/M; 0; -1/(M*l)];
C = [1 0 0 0; 0 0 1 0];  % Output y is [x, theta]
D = 0;


# DEBUG:
disp("[1] State Space Matrices constructed...")
# A, B, C, D

# iif runnig octave (vs Matlab)
pkg load control

% Design state-feedback controller
Q = diag([1, 1, 10, 10]);  % State weighting matrix
R = 0.01;  % Control input weighting
K = lqr(A, B, Q, R);  % Compute optimal state-feedback gain

disp("For Parameters in ControlChallenges, Level 9, ...");
K

#% Closed-loop system
Ac = A - B*K;
Bc = B;
Cc = C;
Dc = D;

% Create state-space model of closed-loop system
sys_cl = ss(Ac, Bc, Cc, Dc);

% Simulate the closed-loop system
t = 0:0.01:5;  % Time vector
x0 = [0; 0; 0.2; 0];  % Initial state [x, x_dot, theta, theta_dot]
u = zeros(size(t));  % No control input (open-loop)
[y, t, x] = lsim(sys_cl, u, t, x0);

% Plot the results
f = figure;
subplot(2,1,1);
plot(t, y(:,1), 'b', 'LineWidth', 2);
xlabel('Time (s)');
ylabel('Cart position (m)');
title('Cart position over time');
grid on;

subplot(2,1,2);

plot(t, y(:,2), 'r', 'LineWidth', 2);
xlabel('Time (s)');
ylabel('Pendulum angle (rad)');
title('Pendulum angle over time');
grid on;

waitfor(f)

