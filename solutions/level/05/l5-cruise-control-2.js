// for use with: https://janismac.github.io/ControlChallenges/
//       source: https://github.com/janismac/ControlChallenges

// Settle Time:

// Simulates for 0.5 seconds. The input for the model is set to be the negative of its `x`.
// ```
//     newState = state.simulate(0.5, function(s){ return -s.x ;})
// ```
// simulate() returns a modified copy of the model state.
// The control function will be evaluated only once at the start of a simulate() call
// and held constant during the simulation, so choose a small dt.

// ==================================================================

const kp = 2.0;
const k_throttle_speed = -2;
const k_throttle_inclination = 1;
const ki = -1.2;
const kd = -24;
const decay = 0.99;

const dt = 0.02; // The simulation time step

let tp; // force, proportional
let integral = 0;
let ti; // force, integrative
let td; // force, derivative
let tt = 0; // force, total

/// \return returns the throttle: [-1,1]
function controlFunction(vehicle) {

    // speed difference
    let ev = (vehicle.speed - vehicle.targetSpeed);

//    vehicle.speed_last = vehicle.speed
    
    // vehicle acceleration
    //let acc = dt * (vehicle.speed - vehicle.speed_last); 
    
    tp = kp*( k_throttle_speed * ev + k_throttle_inclination * vehicle.inclination); 
    

    // according to "standard theory", the integral term is: 
    //    \[ = K_i \int_0^t e(t) \,dt \]
    // Where `e` is the process variable, the difference between the setpoint and the measured process variable.
//    integral = decay * (integral + vehicle.x);

  
    //fi = 0; //ki * integral;
    //fd = kd * acc;
    // ======
    //ft = fp + fi + fd;
  	tt = tp;

    monitor("    vel:    ", vehicle.speed );
    monitor("    err(v): ", ev );

  	monitor("    tp_speed: ", (k_throttle_speed * ev));
    monitor("    tp_incl:  ", (k_throttle_inclination * vehicle.inclination));
    monitor("    tp:       ", tp );
    //monitor("    fi", fi );
    //monitor("      int:", integral)
    //monitor("    fd", fd );
    monitor("Throttle, Total: ", tt );
  
    return tt;
}
