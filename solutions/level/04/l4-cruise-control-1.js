// for use with: https://janismac.github.io/ControlChallenges/
//       source: https://github.com/janismac/ControlChallenges

// Settle Time:

//function controlFunction(vehicle){
//  return 4 * (vehicle.targetSpeed - vehicle.speed);
//};

// Simulates for 0.5 seconds. The input for the model is set to be the negative of its `x`.
// ```
//     newState = state.simulate(0.5, function(s){ return -s.x ;})
// ```
// simulate() returns a modified copy of the model state.
// The control function will be evaluated only once at the start of a simulate() call
// and held constant during the simulation, so choose a small dt.

// ==================================================================

const kp = -2;
const ki = -1.2;
const kd = -24;
const decay = 0.99;

const dt = 0.02; // The simulation time step

let fp; // force, proportional
let integral = 0;
let fi; // force, integrative
let fd; // force, derivative
let ft = 0; // force, total

/// \return returns the throttle: [-1,1]
function controlFunction(vehicle) {

  	// speed difference
  	let ev = ( vehicle.targetSpeed - vehicle.speed) 

    vehicle.speed_last = vehicle.speed
    
    // vehicle acceleration
    let acc = dt * (vehicle.speed - vehicle.speed_last); 
    
    fp = kp*ev;
    
    // vehicle.inclination
    // vehicle.speedHoldTimer
    // vehicle.T

    fp = kp * ev

    // according to "standard theory", the integral term is: 
    //    \[ = K_i \int_0^t e(t) \,dt \]
    // Where `e` is the process variable, the difference between the setpoint and the measured process variable.
//    integral = decay * (integral + vehicle.x);

    fi = 0; //ki * integral;
    fd = kd * acc;
    // ======
    ft = fp + fi + fd;

  
    monitor("    vel:    ", vehicle.speed );
    monitor("    err(v): ", ev );
    monitor("    acc:    ", acc );

    monitor("    fp", fp );
    monitor("    fi", fi );
    monitor("      int:", integral)
    monitor("    fd", fd );
    monitor("Throttle, Total", ft );
  
    return ft;
}
